FROM baza

COPY init_db.sh init.sql entrypoint.sh /

RUN sudo apt-get update && \
    sudo apt-get install dos2unix && \
    dos2unix init_db.sh && \
    dos2unix init.sql && \
    dos2unix entrypoint.sh

ENV DEBIAN_FRONTEND noninteractive
ENV ORACLE_HOME /u01/app/oracle/product/11.2.0/xe
ENV PATH $ORACLE_HOME/bin:$PATH
ENV ORACLE_SID=XE
ENV HOSTNAME=localhost

RUN chown -R oracle:dba /u01/app/oracle && \
rm -f /u01/app/oracle/product && \
ln -s /u01/app/oracle-product /u01/app/oracle/product && \
sed -i -E "s/HOST = [^)]+/HOST = $HOSTNAME/g" /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora && \
sed -i -E "s/PORT = [^)]+/PORT = 1521/g" /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora && \
echo "export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe" > /etc/profile.d/oracle-xe.sh && \
echo "export PATH=\$ORACLE_HOME/bin:\$PATH" >> /etc/profile.d/oracle-xe.sh && \
echo "export ORACLE_SID=XE" >> /etc/profile.d/oracle-xe.sh && \
sh init_db.sh

EXPOSE 1521
EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]