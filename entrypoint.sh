#!/usr/bin/env bash

    /etc/init.d/oracle-xe start

	##
	## Workaround for graceful shutdown. ....ing oracle... ‿( ́ ̵ _-`)‿
	##
	while [ "$END" == '' ]; do
		sleep 1
		trap "/etc/init.d/oracle-xe stop && END=1" INT TERM
	done